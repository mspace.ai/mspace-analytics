import { AppShell, Curves, KPI } from "app/components";
import { Grid, Paper, useTheme } from "@material-ui/core";
import {
  bankHistoriesQuery,
  bankQuery,
  dayDatasQuery,
  metaPriceQuery,
  factoryQuery,
  getApollo,
  getBank,
  getBankHistories,
  getDayData,
  getMetaPrice,
  getFactory,
  getMSpaceToken,
  tokenQuery,
  useInterval,
} from "app/core";

import Chart from "../../components/Chart";
import Head from "next/head";
import { ParentSize } from "@visx/responsive";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useQuery } from "@apollo/client";
import { MSPACE_ADDRESS } from "../../core/constants";

const useStyles = makeStyles((theme) => ({
  charts: {
    flexGrow: 1,
    marginBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    // textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

function BankPage() {
  const classes = useStyles();

  const theme = useTheme();

  const {
    data: { bank },
  } = useQuery(bankQuery, {
    context: {
      clientName: "bank",
    },
  });

  const {
    data: { histories },
  } = useQuery(bankHistoriesQuery, {
    context: {
      clientName: "bank",
    },
  });

  const {
    data: { factory },
  } = useQuery(factoryQuery);

  const {
    data: { token },
  } = useQuery(tokenQuery, {
    variables: {
      id: MSPACE_ADDRESS,
    },
  });

  const {
    data: { bundles },
  } = useQuery(metaPriceQuery);

  const {
    data: { dayDatas },
  } = useQuery(dayDatasQuery);

  const mspacePrice =
    parseFloat(token?.derivedMETA) * parseFloat(bundles[0].metaPrice);

  useInterval(async () => {
    await Promise.all([
      getBank,
      getBankHistories,
      getDayData,
      getFactory,
      getMSpaceToken,
      getMetaPrice,
    ]);
  }, 60000);

  const {
    mspaceStakedUSD,
    mspaceHarvestedUSD,
    xMSpaceMinted,
    xMSpaceBurned,
    xMSpace,
    apr,
    apy,
    fees,
  } = histories.reduce(
    (previousValue, currentValue) => {
      const date = currentValue.date * 1000;
      let dayData = dayDatas.find((d) => d.date === currentValue.date);
      if (dayData == undefined) {
        dayData = dayDatas[0];
      }
      previousValue["mspaceStakedUSD"].push({
        date,
        value: parseFloat(currentValue.mspaceStakedUSD),
      });
      previousValue["mspaceHarvestedUSD"].push({
        date,
        value: parseFloat(currentValue.mspaceHarvestedUSD),
      });

      previousValue["xMSpaceMinted"].push({
        date,
        value: parseFloat(currentValue.xMSpaceMinted),
      });
      previousValue["xMSpaceBurned"].push({
        date,
        value: parseFloat(currentValue.xMSpaceBurned),
      });
      previousValue["xMSpace"].push({
        date,
        value: parseFloat(currentValue.xMSpaceSupply),
      });
      const apr =
        (((dayData.volumeUSD * 0.05 * 0.01) / currentValue.xMSpaceSupply) *
          365) /
        (currentValue.ratio * mspacePrice);
      previousValue["apr"].push({
        date,
        value: parseFloat(apr * 100),
      });
      previousValue["apy"].push({
        date,
        value: parseFloat((Math.pow(1 + apr / 365, 365) - 1) * 100),
      });
      previousValue["fees"].push({
        date,
        value: parseFloat(dayData.volumeUSD * 0.005),
      });
      return previousValue;
    },
    {
      mspaceStakedUSD: [],
      mspaceHarvestedUSD: [],
      xMSpaceMinted: [],
      xMSpaceBurned: [],
      xMSpace: [],
      apr: [],
      apy: [],
      fees: [],
    }
  );

  const averageApy =
    apy.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.value;
    }, 0) / apy.length;

  const oneDayVolume = factory.volumeUSD - factory.oneDay.volumeUSD;

  const APR =
    (((oneDayVolume * 0.05 * 0.01) / bank.totalSupply) * 365) /
    (bank.ratio * mspacePrice);

  const APY = Math.pow(1 + APR / 365, 365) - 1;

  return (
    <AppShell>
      <Head>
        <title>Bank | MSpaceSwap Analytics</title>
      </Head>

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={3}>
              <KPI title="APY (24h)" value={APY * 100} format="percent" />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <KPI title="APY (Avg)" value={averageApy} format="percent" />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <KPI title="xMSP" value={bank.totalSupply} format="integer" />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <KPI title="xMSP:MSP" value={Number(bank.ratio).toFixed(4)} />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Paper
            variant="outlined"
            style={{ display: "flex", height: 400, flex: 1 }}
          >
            <ParentSize>
              {({ width, height }) => (
                <Curves
                  width={width}
                  height={height}
                  margin={{ top: 64, right: 32, bottom: 0, left: 64 }}
                  data={[apy, apr]}
                  labels={["APY", "APR"]}
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper
            variant="outlined"
            style={{ display: "flex", height: 400, flex: 1 }}
          >
            <ParentSize>
              {({ width, height }) => (
                <Curves
                  width={width}
                  height={height}
                  title="Fees received (USD)"
                  margin={{ top: 64, right: 32, bottom: 0, left: 64 }}
                  data={[fees]}
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper
            variant="outlined"
            style={{ display: "flex", height: 400, flex: 1 }}
          >
            <ParentSize>
              {({ width, height }) => (
                <Curves
                  width={width}
                  height={height}
                  data={[mspaceStakedUSD, mspaceHarvestedUSD]}
                  margin={{ top: 64, right: 32, bottom: 0, left: 64 }}
                  labels={["MSP Staked (USD)", "MSP Harvested (USD)"]}
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper
            variant="outlined"
            style={{ display: "flex", height: 400, flex: 1 }}
          >
            <ParentSize>
              {({ width, height }) => (
                <Curves
                  width={width}
                  height={height}
                  margin={{ top: 64, right: 32, bottom: 0, left: 64 }}
                  data={[xMSpaceMinted, xMSpaceBurned]}
                  labels={["xMSP Minted", "xMSP Burned"]}
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <Paper
            variant="outlined"
            style={{ display: "flex", height: 400, flex: 1 }}
          >
            <ParentSize>
              {({ width, height }) => (
                <Curves
                  width={width}
                  height={height}
                  title="xMSP Total Supply"
                  margin={{ top: 64, right: 32, bottom: 0, left: 64 }}
                  data={[xMSpace]}
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>
      </Grid>

      {/* <pre>{JSON.stringify(bar, null, 2)}</pre> */}
    </AppShell>
  );
}

export async function getStaticProps() {
  const client = getApollo();
  await getBank(client);
  await getBankHistories(client);
  await getFactory(client);
  await getDayData(client);
  await getMSpaceToken(client);
  await getMetaPrice(client);
  return {
    props: {
      initialApolloState: client.cache.extract(),
    },
    revalidate: 1,
  };
}

export default BankPage;
