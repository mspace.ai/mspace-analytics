import {
    AppShell,
    LiquidityPositionTable,
    Search,
} from "app/components";
import { Box, Grid, Paper } from "@material-ui/core";
import React, { useState } from "react";
import {
    getApollo,
    // getDayData,
    // getOneDayMetaPrice,
    getPairs,
    // getPools,
    // getSevenDayMetaPrice,
    getTokens,
    getLiquidityPositions,

    pairsQuery,
    liquidityPositionsQuery,
    tokensQuery,
    useInterval,
} from "app/core";

import Head from "next/head";
import { useQuery } from "@apollo/client";

function IndexPage() {
    const {
        data: { tokens },
    } = useQuery(tokensQuery);

    const {
        data: { pairs },
    } = useQuery(pairsQuery);

    const {
        data: { liquidityPositions }
    } = useQuery(liquidityPositionsQuery);

    useInterval(
        () =>
            Promise.all([
                getPairs,
                getTokens,
                getLiquidityPositions
            ]),
        60000
    );

    return (
        <AppShell>
            <Head>
                <title>Dashboard | MSpaceSwap Analytics</title>
            </Head>
            <Box mb={3}>
                <Search pairs={pairs} tokens={tokens} />
            </Box>

            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <LiquidityPositionTable
                        title="Top Liquidity Positions"
                        liquidityPositions={liquidityPositions}
                        rowsPerPage={5}
                    />
                </Grid>
            </Grid>
        </AppShell>
    );
}

export async function getStaticProps() {
    const client = getApollo();

    await getTokens(client);

    await getPairs(client);

    await getLiquidityPositions(client);

    return {
        props: {
            initialApolloState: client.cache.extract(),
        },
        revalidate: 1,
    };
}

export default IndexPage;
