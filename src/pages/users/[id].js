import { 
  AppShell, KPI, Link, Loading, PageHeader, PairIcon,
  TokenIcon,
  UserTransaction,
} from "app/components";
import {
  Avatar,
  Box,
  Grid,
  // Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  makeStyles,
} from "@material-ui/core";
import {
  bankUserQuery,
  // blockQuery,
  currencyFormatter,
  decimalFormatter,
  metaPriceQuery,
  formatCurrency,
  getApollo,
  getBankUser,
  getMetaPrice,
  getLatestBlock,
  getPairs,
  getPoolUser,
  getMSpaceToken,
  // getToken,
  // getUser,
  getUserTransactions,
  latestBlockQuery,
  // lockupUserQuery,
  // pairSubsetQuery,
  pairsQuery,
  poolUserQuery,
  tokenQuery,
  // useInterval,
  // userIdsQuery,
  // userQuery,
  userTransactionsQuery,
  formatSymbol,
} from "app/core";
// import { getUnixTime, startOfMinute, startOfSecond } from "date-fns";

// import { AvatarGroup } from "@material-ui/lab";
import Head from "next/head";
import { POOL_DENY, MSPACE_ADDRESS } from "../../core/constants";
import { toChecksumAddress } from "web3-utils";
import { useQuery } from "@apollo/client";
import { useRouter } from "next/router";
// import { BigNumber } from "@ethersproject/bignumber";
// import JSBI from "jsbi";

const useStyles = makeStyles((theme) => ({
  root: {},
  title: {
    fontSize: 14,
  },
  avatar: {
    marginRight: theme.spacing(1),
  },
  paper: {
    padding: theme.spacing(2),
  },
}));

function UserPage() {
  const router = useRouter();

  if (router.isFallback) {
    return <Loading />;
  }
  const classes = useStyles();

  const id = router && router.query && router.query.id && router.query.id.toLowerCase();

  const {
    data: { bundles },
  } = useQuery(metaPriceQuery, {
    pollInterval: 60000,
  });

  const { data: bankData } = useQuery(bankUserQuery, {
    variables: {
      id: id.toLowerCase(),
    },
    context: {
      clientName: "bank",
    },
  });

  const { data: poolData } = useQuery(poolUserQuery, {
    variables: {
      address: id.toLowerCase(),
    },
    context: {
      clientName: "mining",
    },
  });

  const {
    data: { token },
  } = useQuery(tokenQuery, {
    variables: {
      id: MSPACE_ADDRESS,
    },
  });

  const {
    data: { pairs },
  } = useQuery(pairsQuery);

  const poolUsers = poolData.users.filter(
    (user) =>
      user.pool &&
      !POOL_DENY.includes(user.pool.id) &&
      user.pool.allocPoint !== "0" &&
      pairs.find((pair) => pair?.id === user.pool.pair)
  );

  const {
    data: transactions
  } = useQuery(userTransactionsQuery, {
    variables: {
      user: id.toLowerCase(),
    },
  });

  const mspacePrice =
    parseFloat(token?.derivedMETA) * parseFloat(bundles[0].metaPrice);

  // Space
  const xMSpace = parseFloat(bankData?.user?.xMSpace);
  
  const mspacePending =
    (xMSpace * parseFloat(bankData?.user?.bank?.mspaceStaked)) /
    parseFloat(bankData?.user?.bank?.totalSupply);

  const xMSpaceTransfered =
  bankData?.user?.xMSpaceIn > bankData?.user?.xMSpaceOut
      ? parseFloat(bankData?.user?.xMSpaceIn) -
        parseFloat(bankData?.user?.xMSpaceOut)
      : parseFloat(bankData?.user?.xMSpaceOut) -
        parseFloat(bankData?.user?.xMSpaceIn);

  const mspaceStaked = bankData?.user?.mspaceStaked;

  const mspaceStakedUSD = bankData?.user?.mspaceStakedUSD;

  const mspaceHarvested = bankData?.user?.mspaceHarvested;
  const mspaceHarvestedUSD = bankData?.user?.mspaceHarvestedUSD;

  const mspacePendingUSD = mspacePending > 0 ? mspacePending * mspacePrice : 0;

  const mspaceRoiMSpace =
    mspacePending -
    (parseFloat(bankData?.user?.mspaceStaked) -
      parseFloat(bankData?.user?.mspaceHarvested) +
      parseFloat(bankData?.user?.mspaceIn) -
      parseFloat(bankData?.user?.mspaceOut));

  const mspaceRoiUSD =
    mspacePendingUSD -
    (parseFloat(bankData?.user?.mspaceStakedUSD) -
      parseFloat(bankData?.user?.mspaceHarvestedUSD) +
      parseFloat(bankData?.user?.usdIn) -
      parseFloat(bankData?.user?.usdOut));

  const { data: blocksData } = useQuery(latestBlockQuery, {
    context: {
      clientName: "blocklytics",
    },
  });

  const blockDifference =
    parseInt(blocksData?.blocks[0].number) -
    parseInt(bankData?.user?.createdAtBlock);

  const bankRoiDailyMSpace = (mspaceRoiMSpace / blockDifference) * 6440;

  // POOLS

  const poolsUSD = poolUsers?.reduce((previousValue, currentValue) => {
    const pair = pairs.find((pair) => pair.id == currentValue?.pool?.pair);
    if (!pair) {
      return previousValue;
    }
    const share = Number(currentValue.amount / 1e18) / pair.totalSupply;
    return previousValue + pair.reserveUSD * share;
  }, 0);

  const poolsPendingUSD = poolUsers?.reduce((previousValue, currentValue) => {
    return previousValue + parseFloat(currentValue.poolRewardDebt);
  }, 0) * mspacePrice;

  const [
    poolEntriesUSD,
    poolExitsUSD,
    poolHarvestedUSD,
  ] = poolData?.users.reduce(
    (previousValue, currentValue) => {
      const [entries, exits, harvested] = previousValue;
      return [
        entries + parseFloat(currentValue.entryUSD),
        exits + parseFloat(currentValue.exitUSD),
        harvested + parseFloat(currentValue.mspaceHarvestedUSD),
      ];
    },
    [0, 0, 0]
  );

  // const investments =
    // poolEntriesUSD + mspacePendingUSD + poolsPendingUSD + poolExitsUSD;

  return (
    <AppShell>
      <Head>
        <title>User {id} | MSpaceSwap Analytics</title>
      </Head>

      <PageHeader>
        <Typography variant="h5" component="h1" gutterBottom noWrap>
          Portfolio {id}
        </Typography>
      </PageHeader>

      <Typography
        variant="h6"
        component="h2"
        color="textSecondary"
        gutterBottom
      >
        Bank
      </Typography>

      {!bankData?.user?.bank ? (
        <Box mb={4}>
          <Typography>Address isn't in the bank...</Typography>
        </Box>
      ) : (
        <>
          <Box mb={4}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} md={3}>
                <KPI
                  title="Value"
                  value={formatCurrency(mspacePrice * mspacePending)}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <KPI title="Invested" value={formatCurrency(mspaceStakedUSD)} />
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <KPI
                  title="xMSP"
                  value={Number(xMSpace.toFixed(2)).toLocaleString()}
                />
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <KPI title="Profit/Loss" value={formatCurrency(mspaceRoiUSD)} />
              </Grid>
            </Grid>
          </Box>

          <Box my={4}>
            <TableContainer variant="outlined">
              <Table aria-label="farming">
                <TableHead>
                  <TableRow>
                    <TableCell key="token">Token</TableCell>
                    <TableCell key="staked" align="right">
                      Deposited
                    </TableCell>
                    <TableCell key="harvested" align="right">
                      Withdrawn
                    </TableCell>
                    <TableCell key="pending" align="right">
                      Pending
                    </TableCell>
                    <TableCell key="mspaceRoiYearly" align="right">
                      ROI (Yearly)
                    </TableCell>
                    <TableCell key="mspaceRoiMonthly" align="right">
                      ROI (Monthly)
                    </TableCell>
                    <TableCell key="mspaceRoiDaily" align="right">
                      ROI (Daily)
                    </TableCell>
                    <TableCell key="bankRoiMSpace" align="right">
                      ROI (All-time)
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow key="12">
                    <TableCell component="th" scope="row">
                      <Box display="flex" alignItems="center">
                        <TokenIcon symbol="MSP" />
                        <Link
                          href={`/tokens/${MSPACE_ADDRESS}`}
                          variant="body2"
                          noWrap
                        >
                          MSP
                        </Link>
                      </Box>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {decimalFormatter.format(mspaceStaked)} (
                        {formatCurrency(mspaceStakedUSD)})
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {decimalFormatter.format(mspaceHarvested)} (
                        {formatCurrency(mspaceHarvestedUSD)})
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {Number(mspacePending.toFixed(2)).toLocaleString()} (
                        {formatCurrency(mspacePrice * mspacePending)})
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {bankRoiDailyMSpace ? decimalFormatter.format(bankRoiDailyMSpace * 365) : 0} (
                        {formatCurrency(bankRoiDailyMSpace * 365 * mspacePrice)})
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {bankRoiDailyMSpace ? decimalFormatter.format(bankRoiDailyMSpace * 30) : 0} (
                        {formatCurrency(bankRoiDailyMSpace * 30 * mspacePrice)})
                      </Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography noWrap variant="body2">
                        {bankRoiDailyMSpace ? decimalFormatter.format(bankRoiDailyMSpace) : 0} (
                        {formatCurrency(bankRoiDailyMSpace * mspacePrice)})
                      </Typography>
                    </TableCell>

                    <TableCell align="right">
                      {bankRoiDailyMSpace ? decimalFormatter.format(bankRoiDailyMSpace) : 0} (
                      {formatCurrency(bankRoiDailyMSpace * mspacePrice)})
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </>
      )}

      <Typography
        variant="h6"
        component="h2"
        color="textSecondary"
        gutterBottom
      >
        Pools
      </Typography>

      {!poolData?.users.length ? (
        <Typography>Address isn't farming...</Typography>
      ) : (
        <>
          <Box mb={4}>
            <Grid container spacing={2}>
              <Grid item xs>
                <KPI
                  title="Value"
                  value={formatCurrency(poolsUSD + poolsPendingUSD)}
                />
              </Grid>
              <Grid item xs>
                <KPI title="Invested" value={formatCurrency(poolEntriesUSD)} />
              </Grid>
              <Grid item xs>
                <KPI
                  title="Profit/Loss"
                  value={formatCurrency(
                    poolsUSD +
                      poolExitsUSD +
                      poolHarvestedUSD +
                      poolsPendingUSD -
                      poolEntriesUSD
                  )}
                />
              </Grid>
            </Grid>
          </Box>

          <Box my={4}>
            <TableContainer variant="outlined">
              <Table aria-label="farming">
                <TableHead>
                  <TableRow>
                    <TableCell key="pool">Pool</TableCell>
                    <TableCell key="mlp" align="right">
                      MLP
                    </TableCell>
                    <TableCell key="entryUSD" align="right">
                      Deposited
                    </TableCell>
                    <TableCell key="exitUSD" align="right">
                      Withdrawn
                    </TableCell>
                    <TableCell key="balance" align="right">
                      Balance
                    </TableCell>
                    <TableCell key="value" align="right">
                      Value
                    </TableCell>
                    <TableCell key="pendingMSpace" align="right">
                      MSP Pending
                    </TableCell>
                    <TableCell key="mspaceHarvested" align="right">
                      MSP Harvested
                    </TableCell>
                    <TableCell key="pl" align="right">
                      Profit/Loss
                    </TableCell>
                    {/* <TableCell key="apy" align="right">
                      APY
                    </TableCell> */}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {poolUsers.map((user) => {
                    const pair = pairs.find(
                      (pair) => pair.id == user.pool.pair
                    );
                    const mlp = Number(user.amount / 1e18);

                    const share = mlp / pair.totalSupply;

                    const token0 = pair.reserve0 * share;
                    const token1 = pair.reserve1 * share;

                    return (
                      <TableRow key={user.pool.id}>
                        <TableCell component="th" scope="row">
                          <Box display="flex" alignItems="center">
                            <PairIcon
                              base={pair.token0}
                              quote={pair.token1}
                            />
                            <Link
                              href={`/pools/${user.pool.id}`}
                              variant="body2"
                              noWrap
                            >
                              {formatSymbol(pair.token0.symbol)}-{formatSymbol(pair.token1.symbol)}
                            </Link>
                          </Box>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {decimalFormatter.format(mlp)} MLP
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {currencyFormatter.format(user.entryUSD)}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {currencyFormatter.format(user.exitUSD)}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {decimalFormatter.format(token0)}{" "}
                            {formatSymbol(pair.token0.symbol)} +{" "}
                            {decimalFormatter.format(token1)}{" "}
                            {formatSymbol(pair.token1.symbol)}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {currencyFormatter.format(pair.reserveUSD * share)}
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {decimalFormatter.format(user.pendingMSpace)} ({currencyFormatter.format(user.pendingMSpace*mspacePrice)})
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {decimalFormatter.format(user.mspaceHarvested)} 
                            ({currencyFormatter.format(user.mspaceHarvestedUSD)})
                          </Typography>
                        </TableCell>
                        <TableCell align="right">
                          <Typography noWrap variant="body2">
                            {currencyFormatter.format(
                              parseFloat(pair.reserveUSD * share) 
                              + parseFloat(user.exitUSD)
                              + parseFloat(user.mspaceHarvestedUSD)
                              + parseFloat(user.pendingMSpace * mspacePrice)
                              - parseFloat(user.entryUSD)
                            )}
                          </Typography>
                        </TableCell>
                        {/* <TableCell align="right">23.76%</TableCell> */}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        </>
      )}

      <Typography
        variant="h6"
        component="h2"
        color="textSecondary"
        gutterBottom
      >
        Transactions
      </Typography>

      <Box my={4}>
        <UserTransaction
            title="Transactions"
            transactions={transactions}
            rowsPerPage={10}
        />
      </Box>
    </AppShell>
  );
}

export async function getStaticProps({ params }) {
  const client = getApollo();

  const id = params.id.toLowerCase();

  await getMetaPrice(client);

  await getMSpaceToken(client);

  await getBankUser(id, client);

  await getPoolUser(id, client);

  await getPairs(client);

  await getLatestBlock(client);

  await getUserTransactions(id, client);

  return {
    props: {
      initialApolloState: client.cache.extract(),
    },
    revalidate: 1,
  };
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: true,
  };
}

export default UserPage;
