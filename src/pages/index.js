import {
  AppShell,
  AreaChart,
  BarChart,
  PairTable,
  PoolTable,
  LiquidityPositionTable,
  Search,
  TokenTable,
} from "app/components";
import { Box, Grid, Paper } from "@material-ui/core";
import React, { useState } from "react";
import {
  dayDatasQuery,
  getApollo,
  getDayData,
  getMetaPrice,
  getOneDayMetaPrice,
  getPairs,
  getPools,
  getSevenDayMetaPrice,
  getTokens,
  getLiquidityPositions,

  pairsQuery,
  poolsQuery,
  liquidityPositionsQuery,
  tokensQuery,
  useInterval,
} from "app/core";

import Head from "next/head";
import { ParentSize } from "@visx/responsive";
import { useQuery } from "@apollo/client";

function IndexPage() {
  const {
    data: { tokens },
  } = useQuery(tokensQuery);

  const {
    data: { pairs },
  } = useQuery(pairsQuery);

  const {
    data: { pools },
  } = useQuery(poolsQuery, {
    context: {
      clientName: "mining",
    },
  });

  const {
    data: { liquidityPositions }
  } = useQuery(liquidityPositionsQuery);
  // console.log("liquidityPositions", liquidityPositions);

  const {
    data: { dayDatas },
  } = useQuery(dayDatasQuery);
  // console.log('dayDatasQuery', dayDatasQuery);
  // console.log('dayDatas', dayDatas);

  useInterval(
    () =>
      Promise.all([
        getPairs,
        getPools,
        getTokens,
        getDayData,
        getOneDayMetaPrice,
        getSevenDayMetaPrice,
        getLiquidityPositions
      ]),
    60000
  );

  const [liquidity, volume] = dayDatas
    .filter((d) => d.liquidityUSD !== "0")
    .reduce(
      (previousValue, currentValue) => {
        previousValue[0].unshift({
          date: currentValue.date,
          value: parseFloat(currentValue.liquidityUSD),
        });
        previousValue[1].unshift({
          date: currentValue.date,
          value: parseFloat(currentValue.untrackedVolume),
        });
        return previousValue;
      },
      [[], []]
    );

  return (
    <AppShell>
      <Head>
        <title>Dashboard | MSpaceSwap Analytics</title>
      </Head>
      <Box mb={3}>
        <Search pairs={pairs} tokens={tokens} />
      </Box>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={6}>
          <Paper variant="outlined" style={{ height: 300 }}>
            <ParentSize>
              {({ width, height }) => (
                <AreaChart
                  title="Liquidity"
                  width={width}
                  height={height}
                  data={liquidity}
                  margin={{ top: 125, right: 0, bottom: 0, left: 0 }}
                  tooltipDisabled
                  overlayEnabled
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Paper
            variant="outlined"
            style={{ height: 300, position: "relative" }}
          >
            <ParentSize>
              {({ width, height }) => (
                <BarChart
                  title="Volume"
                  width={width}
                  height={height}
                  data={volume}
                  margin={{ top: 125, right: 0, bottom: 0, left: 0 }}
                  tooltipDisabled
                  overlayEnabled
                />
              )}
            </ParentSize>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <TokenTable
            title="Top Tokens"
            rowsPerPage={5}
            tokens={tokens}
          />
        </Grid>

        <Grid item xs={12}>
          <PairTable
            title="Top Pairs"
            rowsPerPage={5}
            pairs={pairs}
          />
        </Grid>

        <Grid item xs={12}>
          <PoolTable
            title="MSP Reward Pools"
            pools={pools}
            orderBy="tvl"
            order="desc"
            rowsPerPage={5}
          />
        </Grid>
        {/* {JSON.stringify(liquidityPositionSnapshots)} */}
        <Grid item xs={12}>
          <LiquidityPositionTable
            title="Top Liquidity Positions"
            liquidityPositions={liquidityPositions}
            rowsPerPage={5}
          />
        </Grid>
      </Grid>
    </AppShell>
  );
}

export async function getStaticProps() {
  const client = getApollo();

  await getDayData(client);

  await getMetaPrice(client);

  await getOneDayMetaPrice(client);

  await getSevenDayMetaPrice(client);

  await getTokens(client);

  await getPairs(client);

  await getPools(client);

  await getLiquidityPositions(client);

  return {
    props: {
      initialApolloState: client.cache.extract(),
    },
    revalidate: 1,
  };
}

export default IndexPage;
