import { Box, Divider, Typography } from "@material-ui/core";
import { formatCurrency, formatDecimal, formatSymbol } from "app/core";

import Link from "./Link";
import PairIcon from "./PairIcon";
import Percent from "./Percent";
import React from "react";
import SortableTable from "./SortableTable";
import TokenIcon from "./TokenIcon";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {},
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
}));

export default function LiquidityPositionTable({ liquidityPositions, ...rest }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <SortableTable
        orderBy="reserveUSD"
        order="desc"
        title="Liquidity Position"
        columns={[
          {
            key: "user",
            label: "Account",
            render: (row, index) => {
              const userAddress = row.user?.id;
              return (
                <Box display="flex" alignItems="center">
                  <Link href={`/users/${userAddress}`} variant="body2" noWrap>
                    {userAddress}
                  </Link>
                </Box>
              );
            },
          },
          {
            key: "pair",
            label: "Pair",
            render: (row, index) => {
              const name = `${formatSymbol(row.pair?.token0?.symbol)}-${formatSymbol(row.pair?.token1?.symbol)}`;
              return (
                <Box display="flex" alignItems="center">
                  <PairIcon
                    base={row.pair?.token0}
                    quote={row.pair?.token1}
                  />
                  <Link href={`/pools/${row.pair?.id}`} variant="body2" noWrap>
                    {name}
                  </Link>
                </Box>
              );
            },
          },
          {
            key: "reserveUSD",
            label: "Value",
            render: (row) =>
              `${Number(row.reserveUSD).toFixed(2)} USD`,
          },
        ]}
        rows={liquidityPositions}
        {...rest}
      />
    </div>
  );
}
