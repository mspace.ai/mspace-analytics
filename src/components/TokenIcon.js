import { Avatar } from "@material-ui/core";
// import HelpIcon from "@material-ui/icons/Help";
import { makeStyles } from "@material-ui/core/styles";
import { toChecksumAddress } from "web3-utils";
import { useMemo } from "react";
const useStyles = makeStyles((theme) => ({
  root: {
    marginRight: theme.spacing(2),
    background: theme.palette.common.white,
    color: theme.palette.text.secondary,
  },
}));

export default function TokenIcon({ id, symbol, ...rest }) {
  const classes = useStyles();
  const src = useMemo(
    () =>
      `/assets/${symbol ? symbol.toLowerCase() : toChecksumAddress(id) + '/logo'}.png`,
    [id]
  );
  return <Avatar classes={{ root: classes.root }} src={src} {...rest} />;
}
