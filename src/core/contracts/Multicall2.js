import { useContract } from "./useContract";
import MULTICALL2_ABI from "./abis/multicall2.json";
import { MULTICALL2_ADDRESS } from "../constants";

export async function fetchChunk(multicallContract, chunk) {
  let resultsBlockNumber, resultsBlockHash, resultsData
  try {
    [resultsBlockNumber, resultsBlockHash, resultsData] = await multicallContract.callStatic.tryBlockAndAggregate(
      false,
      chunk.map(obj => [obj.address, obj.callData])
    )
  } catch (error) {
    console.debug('Failed to fetch chunk inside retry', error)
    throw error
  }
  return {
    results: resultsData.map(result => result.success && result.returnData != "0x" ? result.returnData:undefined), 
    blockNumber: resultsBlockNumber.toNumber()
  }
}

export function encodeCalls(contract, methodName, callInputs) {
  const fragment = contract?.interface?.getFunction(methodName);

  return contract && fragment && callInputs && callInputs.length > 0 ? 
    callInputs.map(inputs => {
      return {
        address: contract.address,
        callData: contract.interface.encodeFunctionData(fragment, inputs)
      }
    }) : []
}

export function decodeCallResults(callResults, contract, methodName) {
  const fragment = contract?.interface?.getFunction(methodName);
  return contract && fragment && callResults && callResults.length > 0 ? 
    callResults.map(data => 
      contract.interface.decodeFunctionResult(fragment, data)
    ):[]
}

export function useMulticallContract() {
  return useContract(MULTICALL2_ADDRESS, MULTICALL2_ABI)
}