import { Contract } from "@ethersproject/contracts";
import { getAddress } from "@ethersproject/address";
import { JsonRpcProvider } from "@ethersproject/providers";
import { AddressZero } from "@ethersproject/constants";
import { PROVIDER_RPC } from '../constants';

export function isAddress(value) {
  try {
    return getAddress(value)
  } catch {
    return false
  }
}

export function getContract(address, ABI, library) {
  if (!isAddress(address) || address === AddressZero) {
    throw Error(`Invalid 'address' parameter '${address}'.`)
  }

  return new Contract(address, ABI, library)
}

export function getNetworkProvider() {
  const provider = new JsonRpcProvider(PROVIDER_RPC);
  return provider;
}

export function useContract(address, ABI) {
  const library = getNetworkProvider();
  if (!address || !ABI || !library) return null
  try {
    return getContract(address, ABI, library);
  } catch (error) {
    console.error('Failed to get contract', error);
    return null;
  }
}
