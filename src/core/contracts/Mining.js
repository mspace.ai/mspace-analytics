import { useContract } from "./useContract"; 
import { MINING_ADDRESS } from "../constants";
import MINING_ABI from "./abis/mining.json";

export function useMiningContract() {
  return useContract(MINING_ADDRESS, MINING_ABI);
}