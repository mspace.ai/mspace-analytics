import { getAverageBlockTime, getMetaPrice, getToken } from "../api";
import {
  liquidityPositionSubsetQuery,
  pairQuery,
  pairSubsetQuery,
} from "../queries/exchange";
import {
  poolHistoryQuery,
  poolIdsQuery,
  poolQuery,
  poolUserQuery,
  poolsQuery,
} from "../queries/mining";
import { useMiningContract } from "../contracts/Mining";
import { useMulticallContract, fetchChunk, encodeCalls, decodeCallResults } from "../contracts/Multicall2";

import { MINING_ADDRESS, POOL_DENY, MSPACE_ADDRESS } from "app/core/constants";
import { getApollo } from "../apollo";
import { sub } from "date-fns";
import { ethers } from "ethers";

export async function getPoolIds(client = getApollo()) {
  const {
    data: { pools },
  } = await client.query({
    query: poolIdsQuery,
    context: {
      clientName: "mining",
    },
  });
  await client.cache.writeQuery({
    query: poolIdsQuery,
    data: {
      pools: pools.filter(
        (pool) => !POOL_DENY.includes(pool.id) && pool.allocPoint !== "0"
      ),
    },
  });
  return await client.cache.readQuery({
    query: poolIdsQuery,
  });
}

export async function getPoolUser(id, client = getApollo()) {
  const { data: {users} } = await client.query({
    query: poolUserQuery,
    fetchPolicy: "network-only",
    variables: {
      address: id,
    },
    context: {
      clientName: "mining",
    },
  });

  const mining = useMiningContract();
  
  const pendingMSpaceResult = await fetchChunk(useMulticallContract(), encodeCalls(
    mining, "pendingMSpace", 
    users.map(user => [user.pool.id, user.address])
  ));
  const usersPendingMSpace = decodeCallResults(pendingMSpaceResult.results, mining, "pendingMSpace");

  const rewardDebtResult = await fetchChunk(useMulticallContract(), encodeCalls(
    mining, "rewardDebt", 
    users.map(user => [user.pool.id])
  ));
  const poolsRewardDebt = decodeCallResults(rewardDebtResult.results, mining, "rewardDebt");

  await client.cache.writeQuery({
    query: poolUserQuery,
    data: {
      users: users.map((user, index) => {
        return {
          ...user,
          pendingMSpace: ethers.utils.formatUnits(usersPendingMSpace[index][0], 18),
          poolRewardDebt: ethers.utils.formatUnits(poolsRewardDebt[index][0], 18)
        }
      })
    },
  });

  return await client.cache.readQuery({
    query: poolUserQuery,
  });
}

export async function getPoolHistories(id, client = getApollo()) {
  const {
    data: { poolHistories },
  } = await client.query({
    query: poolHistoryQuery,
    fetchPolicy: "network-only",
    variables: { id },
    context: {
      clientName: "mining",
    },
  });

  await client.cache.writeQuery({
    query: poolHistoryQuery,
    data: {
      poolHistories,
    },
  });

  return await client.cache.readQuery({
    query: poolHistoryQuery,
  });
}

export async function getPool(id, client = getApollo()) {
  const {
    data: { pool },
  } = await client.query({
    query: poolQuery,
    fetchPolicy: "network-only",
    variables: { id },
    context: {
      clientName: "mining",
    },
  });

  const {
    data: { pair: liquidityPair },
  } = await client.query({
    query: pairQuery,
    variables: { id: pool.pair },
    fetchPolicy: "network-only",
  });

  await client.cache.writeQuery({
    query: poolQuery,
    data: {
      pool: {
        ...pool,
        liquidityPair,
      },
    },
  });

  return await client.cache.readQuery({
    query: poolQuery,
  });
}

export async function getPools(client = getApollo()) {
  const {
    data: { pools },
  } = await client.query({
    query: poolsQuery,
    context: {
      clientName: "mining",
    },
  });

  const pairAddresses = pools
    .map((pool) => {
      return pool.pair;
    })
    .sort();

  const pool45 = pools.find((p) => p.id === "45");

  const {
    data: { pairs },
  } = await client.query({
    query: pairSubsetQuery,
    variables: { pairAddresses },
    fetchPolicy: "network-only",
  });

  // const averageBlockTime = (await getAverageBlockTime()) / 100;

  const averageBlockTime = await getAverageBlockTime();
  // const averageBlockTime = 13;

  const { bundles } = await getMetaPrice();

  const metaPrice = bundles[0].metaPrice;

  const { token } = await getToken(MSPACE_ADDRESS);

  const mspacePrice = metaPrice * token.derivedMETA;

  // mining
  const {
    data: { liquidityPositions },
  } = await client.query({
    query: liquidityPositionSubsetQuery,
    variables: { user: MINING_ADDRESS },
  });

  const mining = useMiningContract();
  const mspacePerBlock = parseFloat(ethers.utils.formatUnits(await mining.mspacePerBlock(), 18));

  await client.cache.writeQuery({
    query: poolsQuery,
    data: {
      pools: pools
        .filter(
          (pool) =>
            !POOL_DENY.includes(pool.id) &&
            pool.allocPoint !== "0" &&
            // pool.accSushiPerShare !== "0" &&
            pairs.find((pair) => pair?.id === pool.pair)
        )
        .map((pool) => {
          const pair = pairs.find((pair) => pair.id === pool.pair);

          const liquidityPosition = liquidityPositions.find(
            (liquidityPosition) => liquidityPosition.pair.id === pair.id
          );

          const balance = Number(pool.balance / 1e18);

          const blocksPerHour = 3600 / averageBlockTime;

          const balanceUSD = (balance / Number(pair.totalSupply)) * Number(pair.reserveUSD);

          // TODO change this
          const rewardPerBlock = ((pool.allocPoint / pool.owner.totalAllocPoint) * mspacePerBlock);
          // console.log(pool.allocPoint, pool.owner.totalAllocPoint, mspacePerBlock, rewardPerBlock, balanceUSD);

          const roiPerBlock = (rewardPerBlock * mspacePrice) / balanceUSD;

          const roiPerHour = roiPerBlock * blocksPerHour;

          const roiPerDay = roiPerHour * 24;

          const roiPerMonth = roiPerDay * 30;

          const roiPerYear = roiPerMonth * 12;

          return {
            ...pool,
            liquidityPair: pair,
            roiPerBlock,
            roiPerHour,
            roiPerDay,
            roiPerMonth,
            roiPerYear,
            rewardPerThousand: 1 * roiPerDay * (1000 / mspacePrice),
            tvl:
              (pair.reserveUSD / pair.totalSupply) *
              liquidityPosition.liquidityTokenBalance,
          };
        }),
    },
  });

  return await client.cache.readQuery({
    query: poolsQuery,
  });
}
