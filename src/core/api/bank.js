import { bankHistoriesQuery, bankQuery, bankUserQuery } from "../queries/bank";

import { getApollo } from "../apollo";

export async function getBank(client = getApollo()) {
  const { data } = await client.query({
    query: bankQuery,
    context: {
      clientName: "bank",
    },
  });

  await client.cache.writeQuery({
    query: bankQuery,
    data,
  });

  return await client.cache.readQuery({
    query: bankQuery,
  });
}

export async function getBankHistories(client = getApollo()) {
  const { data } = await client.query({
    query: bankHistoriesQuery,
    context: {
      clientName: "bank",
    },
  });

  await client.cache.writeQuery({
    query: bankHistoriesQuery,
    data,
  });

  return await client.cache.readQuery({
    query: bankHistoriesQuery,
  });
}

export async function getBankUser(id, client = getApollo()) {
  const { data } = await client.query({
    query: bankUserQuery,
    variables: {
      id,
    },
    context: {
      clientName: "bank",
    },
  });

  await client.cache.writeQuery({
    query: bankUserQuery,
    data,
  });

  return await client.cache.readQuery({
    query: bankUserQuery,
  });
}
