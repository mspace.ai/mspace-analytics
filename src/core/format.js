import { timeFormat } from "d3-time-format";

const locales = ["en-US"];

export const currencyFormatter = new Intl.NumberFormat(locales, {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
});

export const decimalFormatter = new Intl.NumberFormat(locales, {
  style: "decimal",
  minimumSignificantDigits: 1,
  maximumSignificantDigits: 4,
});

export const formatDate = timeFormat("%b %d, '%y");

export function formatCurrency(value) {
  return currencyFormatter.format(value || 0);
}

export function formatDecimal(value) {
  return decimalFormatter.format(value || 0);
}

export function formatAddress(value) {
  return value;
}

export const formatSymbol = symbol => {
  if (symbol === 'WMETA') {
    return 'META'
  }
  const str = symbol === 'WETH' ? 'ETH' : symbol
  return !['MSP', 'xMSP'].includes(str) ? 'm' + str : str
}
