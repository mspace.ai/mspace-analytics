import { HttpLink, from, split } from "@apollo/client";

import { RetryLink } from "@apollo/client/link/retry";

export const bank = from([
  new RetryLink(),
  new HttpLink({
    uri: `${process.env.SUBGRAPHS_API}/api/subgraphs/name/mspace/bank`,
    shouldBatch: true,
  }),
]);

export const mining = from([
  new RetryLink(),
  new HttpLink({
    uri: `${process.env.SUBGRAPHS_API}/api/subgraphs/name/mspace/mining`,
    shouldBatch: true,
  }),
]);

export const exchange = from([
  new RetryLink(),
  new HttpLink({
    uri: `${process.env.SUBGRAPHS_API}/api/subgraphs/name/mspace/exchange`,
    shouldBatch: true,
  }),
]);

export const blocklytics = from([
  new RetryLink(),
  new HttpLink({
    uri: `${process.env.SUBGRAPHS_API}/api/subgraphs/name/mspace/blocks`,
    shouldBatch: true,
  }),
]);

export const lockup = from([
  new RetryLink(),
  new HttpLink({
    uri: `${process.env.SUBGRAPHS_API}/api/subgraphs/name/mspace/lockup`,
    shouldBatch: true,
  }),
]);

export default split(
  (operation) => {
    return operation.getContext().clientName === "blocklytics";
  },
  blocklytics,
  split(
    (operation) => {
      return operation.getContext().clientName === "mining";
    },
    mining,
    split(
      (operation) => {
        return operation.getContext().clientName === "bank";
      },
      bank,
      split(
        (operation) => {
          return operation.getContext().clientName === "lockup";
        },
        lockup,
        exchange
      )
    )
  )
);
