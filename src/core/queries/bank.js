import gql from "graphql-tag";

import { BANK_ADDRESS } from "../constants";

export const bankQuery = gql`
  query bankQuery($id: String! = "${BANK_ADDRESS}") {
    bank(id: $id) {
      id
      totalSupply
      ratio
      xMSpaceMinted
      xMSpaceBurned
      mspaceStaked
      mspaceStakedUSD
      mspaceHarvested
      mspaceHarvestedUSD
      xMSpaceAge
      xMSpaceAgeDestroyed
      # histories(first: 1000) {
      #   id
      #   date
      #   timeframe
      #   mspaceStaked
      #   mspaceStakedUSD
      #   mspaceHarvested
      #   mspaceHarvestedUSD
      #   xMSpaceAge
      #   xMSpaceAgeDestroyed
      #   xMSpaceMinted
      #   xMSpaceBurned
      #   xMSpaceSupply
      #   ratio
      # }
    }
  }
`;

export const bankHistoriesQuery = gql`
  query bankHistoriesQuery {
    histories(first: 1000) {
      id
      date
      timeframe
      mspaceStaked
      mspaceStakedUSD
      mspaceHarvested
      mspaceHarvestedUSD
      xMSpaceAge
      xMSpaceAgeDestroyed
      xMSpaceMinted
      xMSpaceBurned
      xMSpaceSupply
      ratio
    }
  }
`;

export const bankUserQuery = gql`
  query bankUserQuery($id: String!) {
    user(id: $id) {
      id
      bank {
        totalSupply
        mspaceStaked
      }
      xMSpace
      mspaceStaked
      mspaceStakedUSD
      mspaceHarvested
      mspaceHarvestedUSD
      xMSpaceIn
      xMSpaceOut
      xMSpaceOffset
      xMSpaceMinted
      xMSpaceBurned
      mspaceIn
      mspaceOut
      usdIn
      usdOut
      updatedAt
    }
  }
`;
