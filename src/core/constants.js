export const TOKEN_DENY = [];

export const PAIR_DENY = [];

export const SYMBOL_DENY = ['xMSP'];

export const EXCHANGE_CREATED_TIMESTAMP = Number(process.env.NEXT_PUBLIC_EXCHANGE_CREATED_TIMESTAMP.toLowerCase());

export const POOL_DENY = [];

export const MSPACE_ADDRESS = process.env.NEXT_PUBLIC_MSPACE_ADDRESS.toLowerCase();

export const MINING_ADDRESS = process.env.NEXT_PUBLIC_MINING_ADDRESS.toLowerCase();

export const BANK_ADDRESS = process.env.NEXT_PUBLIC_BANK_ADDRESS.toLowerCase();

export const FACTORY_ADDRESS = process.env.NEXT_PUBLIC_FACTORY_ADDRESS.toLowerCase();

export const PROVIDER_RPC = process.env.NEXT_PUBLIC_PROVIDER_RPC;

export const MULTICALL2_ADDRESS = process.env.NEXT_PUBLIC_MULTICALL2_ADDRESS.toLowerCase();