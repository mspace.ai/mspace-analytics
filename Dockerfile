FROM node:16 as dependencies
WORKDIR /app
COPY ./package.json ./
RUN yarn

FROM node:16 as runner
WORKDIR /app
RUN addgroup --gid 1001 nodejs
RUN adduser --system nextjs -u 1001
RUN yarn add next@^10.0.0 next-pwa@^3.1.4 react@^17.0.0 react-dom@^17.0.0
COPY --from=dependencies /app/node_modules ./node_modules
COPY . .
RUN yarn build && rm -Rf /app/src
RUN chown -R nextjs:nodejs ./.next
USER nextjs
EXPOSE 3000
CMD ["npx", "next", "start"]
